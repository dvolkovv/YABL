package com.volkov.yabl

import android.content.res.Resources
import com.google.common.truth.Truth.assertThat
import com.volkov.yabl.utils.TimeFormatter
import junit.framework.TestCase
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.util.regex.Pattern

class TimeFormatterTest : TestCase() {

    private val resMock = mock(Resources::class.java)
    private var formatter: TimeFormatter = TimeFormatter(resMock)

    @Test
    fun test_formatTimeValid() {
        val testPattern = Pattern.compile("^([1-9]{1,3}:)?\\d{1,2}:\\d\\d\$")
        val timesInMilliseconds = intArrayOf(
            0,
            999,
            5000,
            100_000,
            1_000_000,
            36_500_00,
            1_000_000_000,
            2_000_000_000,
            2_070_000_000,
            Int.MAX_VALUE
        )
        timesInMilliseconds.forEach { time ->
            val formattedTime = formatter.formatBookTime(time)
            println(formattedTime)
            assertThat(
                formattedTime
            ).matches(testPattern)
        }
    }

    @Test
    fun test_formatFullTimeValid() {
        val testPattern = Pattern.compile("^\\d{1,2}:?\\d{1,2}:\\d\\d/\\d{1,2}:?\\d{1,2}:\\d\\d$")
        val arg1 = formatter.formatBookTime(0)
        val arg2 = formatter.formatBookTime(100_000)
        `when`(
            resMock.getString(
                R.string.elapsed_time,
                arg1,
                arg2
            )
        ).thenReturn(String.format("%1s/%2s", arg1, arg2))
        val formattedTime = formatter.getProgressTimeString(0, 100000)
        assertThat(formattedTime).matches(testPattern)
    }

}
