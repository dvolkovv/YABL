package com.volkov.yabl

import android.net.Uri
import com.google.common.truth.Truth.assertThat
import com.volkov.db.dao.CurrentBookDao
import com.volkov.db.dao.LastBooksDao
import com.volkov.db.entity.BookEntity
import com.volkov.db.entity.CurrentBookEntity
import com.volkov.db.entity.LastBookEntity
import com.volkov.filemanager.FileManager
import com.volkov.yabl.data.repository.BookRepository
import com.volkov.yabl.data.repository.IBookRepository
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner
import java.util.*


@RunWith(RobolectricTestRunner::class)
class BookRepositoryTest : TestCase() {

    private val lastBookDaoMock = mock(LastBooksDao::class.java)
    private val currentBookDaoMock = mock(CurrentBookDao::class.java)
    private val fileManagerMock = mock(FileManager::class.java)
    private val bookRepository: IBookRepository =
        BookRepository(lastBookDaoMock, currentBookDaoMock, fileManagerMock)

    @Test
    fun test_lastBooksReturnsEmptyList() = runBlocking {
        Mockito.`when`(lastBookDaoMock.getAll()).thenReturn(emptyList())

        assertThat(bookRepository.getAllLastBooks()).isEmpty()
    }

    @Test
    fun test_lastBooksReturnsUnmodifiedList() = runBlocking {
        val mockedResults = List(
            10
        ) { LastBookEntity(0, "helloworld", 1000, Date(), Uri.parse("/")) }
        Mockito.`when`(lastBookDaoMock.getAll()).thenReturn(mockedResults)

        assertThat(bookRepository.getAllLastBooks()).isEqualTo(mockedResults)
    }

    fun test_getCurrentBookNull() = runBlocking {
        Mockito.`when`(currentBookDaoMock.getCurrentBook()).thenReturn(null)

        assertThat(bookRepository.getCurrentBook()).isNull()
    }

    @Test
    fun test_getCurrentBookReturnsValid() = runBlocking {
        val mockedBook = CurrentBookEntity(0, 0, "", 0, Uri.parse("/"))

        Mockito.`when`(currentBookDaoMock.getCurrentBook()).thenReturn(mockedBook)

        assertThat(bookRepository.getCurrentBook()).isInstanceOf(BookEntity::class.java)
    }
}