package com.volkov.yabl

import android.app.Application
import com.volkov.yabl.di.DaggerComponentsHandler

class YABL : Application() {

    companion object {
        lateinit var components: DaggerComponentsHandler
    }

    override fun onCreate() {
        super.onCreate()
        components = DaggerComponentsHandler(this)
        components.onCreate()
        components.appComponent.settingsManager().setSavedNightMode()
    }
}