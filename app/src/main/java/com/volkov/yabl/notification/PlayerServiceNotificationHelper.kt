package com.volkov.yabl.notification

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.RemoteViews
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.core.app.NotificationCompat
import com.volkov.yabl.R
import com.volkov.yabl.player.state.BookPlayerState
import com.volkov.yabl.player.state.PausedState
import com.volkov.yabl.player.state.PlayingState
import com.volkov.yabl.player.state.ReadyState
import com.volkov.yabl.service.IPlayerService
import com.volkov.yabl.service.PlayerService
import com.volkov.yabl.utils.TimeFormatter
import com.volkov.yabl.view.ui.main.MainActivity
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

const val NOTIFICATION_ID = 1332
private const val PLAY_CLICK_ACTION = "PLAY_CLICK_ACTION"
private const val PAUSE_CLICK_ACTION = "PAUSE_CLICK_ACTION"
private const val REWIND_CLICK_ACTION = "REWIND_CLICK_ACTION"
private const val FORWARD_CLICK_ACTION = "FORWARD_CLICK_ACTION"
private const val CLOSE_CLICK_ACTION = "CLOSE_CLICK_ACTION"
private const val NOTIFICATION_CHANNEL_ID = NOTIFICATION_ID.toString()

@Singleton
class PlayerServiceNotificationHelper @Inject constructor(
    private var service: IPlayerService,
    @Named("ServiceContext")
    private var context: Context,
    private val timeFormatter: TimeFormatter
) {

    var bookDuration: Int = 0
    private var notificationBuilder: NotificationCompat.Builder? = null
    private val notificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


    /**
     * Checks if intent contains information about click on notification button
     * @return true if intent action is caught, false otherwise
     */
    fun onIntentReceived(intent: Intent): Boolean {
        return when (intent.action) {
            PLAY_CLICK_ACTION -> {
                service.play()
                true
            }
            PAUSE_CLICK_ACTION -> {
                service.pause()
                true
            }
            REWIND_CLICK_ACTION -> {
                service.rewind()
                true
            }
            FORWARD_CLICK_ACTION -> {
                service.forward()
                true
            }
            CLOSE_CLICK_ACTION -> {
                service.close()
                true
            }
            else -> false
        }
    }

    fun buildServiceNotification(): Notification {
        return context.buildServiceNotification()
    }

    fun onPlayerStateChanged(newState: BookPlayerState) {
        when (newState) {
            is PlayingState, is ReadyState, is PausedState -> changePlayButton(newState)
            else -> return
        }
    }

    fun setTime(time: Int) {
        val newTimeSpanned = timeFormatter.getProgressTimeString(time, bookDuration)
        changeNotificationViewContent { it.changeText(R.id.elapsed_time, newTimeSpanned) }
    }

    fun setBookTitle(title: String?) {
        changeNotificationViewContent { it.changeText(R.id.bookTitle, title) }
    }

    private fun changePlayButton(playerState: BookPlayerState) {
        when (playerState) {
            is PlayingState -> {
                changeNotificationViewContent {
                    it.changeImage(R.id.play, R.drawable.ic_pause)
                    it.setNotificationButtonClickListener(context, PAUSE_CLICK_ACTION, R.id.play)
                }
            }
            is PausedState, is ReadyState -> {
                changeNotificationViewContent {
                    it.changeImage(R.id.play, R.drawable.ic_play_arrow)
                    it.setNotificationButtonClickListener(context, PLAY_CLICK_ACTION, R.id.play)
                }
            }
            else -> return
        }
    }

    private fun RemoteViews.changeImage(@IdRes viewId: Int, @DrawableRes drawableRes: Int) {
        setImageViewResource(viewId, drawableRes)
    }

    private fun RemoteViews.changeText(@IdRes viewId: Int, newText: CharSequence?) {
        setTextViewText(viewId, newText ?: "")
    }

    /**
     * Getting all required managers and notifying about notification change
     */
    private fun changeNotificationViewContent(updateFunc: (notificationLayout: RemoteViews) -> Unit) {
        val notificationLayout = notificationBuilder?.contentView ?: return
        updateFunc.invoke(notificationLayout)
        val updatedNotification: Notification = notificationBuilder?.build() ?: return
        notificationManager.notify(NOTIFICATION_ID, updatedNotification)
    }

    private fun Context.buildServiceNotification(): Notification {
        val notificationIntent = Intent(this, MainActivity::class.java)
        val notificationLayout = RemoteViews(packageName, R.layout.notification_small)

        val pendingIntent = PendingIntent.getActivity(
            this, 0,
            notificationIntent, 0
        )
        notificationLayout.let {
            it.setNotificationButtonClickListener(this, PLAY_CLICK_ACTION, R.id.play)
            it.setNotificationButtonClickListener(this, FORWARD_CLICK_ACTION, R.id.forward)
            it.setNotificationButtonClickListener(this, REWIND_CLICK_ACTION, R.id.rewind)
            it.setNotificationButtonClickListener(this, CLOSE_CLICK_ACTION, R.id.close)
        }
        val builder = getNotificationBuilder()
            .setCustomContentView(notificationLayout)
            .setSmallIcon(R.drawable.ic_notification_small)
            .setContentIntent(pendingIntent)
        notificationBuilder = builder

        return builder.build()
    }

    private fun getNotificationBuilder(): NotificationCompat.Builder {
        val notificationBuilder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            prepareChannel()
        }
        return notificationBuilder
    }

    @TargetApi(26)
    private fun prepareChannel() {
        val id = NOTIFICATION_CHANNEL_ID
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val appName = context.getString(R.string.app_name)
        val description = context.getString(R.string.notifications_channel_description)

        var nChannel: NotificationChannel? = notificationManager.getNotificationChannel(id)

        if (nChannel == null) {
            nChannel = NotificationChannel(id, appName, importance)
            nChannel.description = description
            nChannel.setSound(null, null)
            nChannel.enableLights(false)
            nChannel.enableVibration(false)
            notificationManager.createNotificationChannel(nChannel)
        }
    }

    private fun RemoteViews.setNotificationButtonClickListener(
        context: Context,
        actionName: String,
        @IdRes viewId: Int
    ) {
        val intent = Intent(context, PlayerService::class.java)
        intent.action = actionName
        val pendingIntent =
            PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        setOnClickPendingIntent(viewId, pendingIntent)
    }

}