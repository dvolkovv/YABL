package com.volkov.yabl.di.module

import com.volkov.yabl.player.BookPlayer
import com.volkov.yabl.player.IBookPlayer
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PlayerModule {

    @Provides
    @Singleton
    fun provideBookPlayer(bookPlayer: BookPlayer): IBookPlayer = bookPlayer
}