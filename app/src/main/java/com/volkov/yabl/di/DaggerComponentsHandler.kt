package com.volkov.yabl.di

import android.content.Context
import com.volkov.yabl.YABL
import com.volkov.yabl.di.component.AppComponent
import com.volkov.yabl.di.component.DaggerAppComponent
import com.volkov.yabl.di.component.PlayerServiceComponent
import com.volkov.yabl.di.module.ServiceModule
import com.volkov.yabl.service.IPlayerService

class DaggerComponentsHandler(private val app: YABL) {
    lateinit var appComponent: AppComponent
        private set

    private var playerComponent: PlayerServiceComponent? = null

    fun onCreate() {
        appComponent = initAppComponent()
    }

    fun clearServiceComponent() {
        playerComponent = null
    }

    fun addServiceComponent(
        playerService: IPlayerService,
        serviceContext: Context
    ): PlayerServiceComponent? {
        playerComponent = playerComponent ?: appComponent.addPlayerServiceComponent(
            ServiceModule(
                playerService,
                serviceContext
            )
        )
        return playerComponent
    }

    private fun initAppComponent() =
        DaggerAppComponent.builder()
            .application(app)
            .build()
}