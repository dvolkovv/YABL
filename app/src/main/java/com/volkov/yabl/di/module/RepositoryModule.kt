package com.volkov.yabl.di.module

import com.volkov.yabl.data.repository.BookRepository
import com.volkov.yabl.data.repository.IBookRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun provideBooksRepository(repo: BookRepository): IBookRepository

}