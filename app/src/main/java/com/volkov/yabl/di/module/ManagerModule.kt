package com.volkov.yabl.di.module

import android.content.Context
import com.volkov.filemanager.FileManager
import com.volkov.yabl.settings.SettingsManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ManagerModule {

    @Provides
    fun provideFileManager(appContext: Context) = FileManager(appContext)

    @Provides
    @Singleton
    fun provideSettingsManager(context: Context) = SettingsManager(context)
}