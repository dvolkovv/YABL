package com.volkov.yabl.di.component

import com.volkov.yabl.di.module.ServiceModule
import com.volkov.yabl.di.scopes.PlayerServiceScope
import com.volkov.yabl.notification.PlayerServiceNotificationHelper
import com.volkov.yabl.service.PlayerService
import dagger.Subcomponent

@Subcomponent(modules = [ServiceModule::class])
@PlayerServiceScope
interface PlayerServiceComponent {
    fun serviceNotificationHelper(): PlayerServiceNotificationHelper

    fun inject(service: PlayerService)
}