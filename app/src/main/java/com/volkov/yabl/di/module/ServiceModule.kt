package com.volkov.yabl.di.module

import android.content.Context
import com.volkov.yabl.notification.PlayerServiceNotificationHelper
import com.volkov.yabl.service.IPlayerService
import com.volkov.yabl.utils.TimeFormatter
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class ServiceModule(private val playerService: IPlayerService, private val context: Context) {

    @Provides
    fun provideService() = playerService

    @Provides
    @Named("ServiceContext")
    fun provideServiceContext() = context

    @Provides
    fun provideServiceNotificationHelper(
        playerService: IPlayerService,
        context: Context,
        timeFormatter: TimeFormatter
    ): PlayerServiceNotificationHelper =
        PlayerServiceNotificationHelper(playerService, context, timeFormatter)
}