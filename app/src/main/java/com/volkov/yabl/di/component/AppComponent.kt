package com.volkov.yabl.di.component

import android.app.Application
import com.volkov.yabl.di.module.*
import com.volkov.yabl.settings.SettingsManager
import com.volkov.yabl.view.adapter.LastBooksAdapter
import com.volkov.yabl.view.custom.BookPlayerWidget
import com.volkov.yabl.view.ui.main.MainPresenter
import com.volkov.yabl.view.ui.settings.SettingsPresenter
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [AppModule::class,
        UtilsModule::class,
        DatabaseModule::class,
        ModelsModule::class,
        RepositoryModule::class,
        ManagerModule::class,
        PlayerModule::class
    ]
)
@Singleton
interface AppComponent {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun addPlayerServiceComponent(serviceModule: ServiceModule): PlayerServiceComponent

    fun settingsManager(): SettingsManager

    fun inject(presenter: MainPresenter)
    fun inject(settingsPresenter: SettingsPresenter)
    fun inject(adapter: LastBooksAdapter)
    fun inject(playerWidget: BookPlayerWidget)
}