package com.volkov.yabl.di.module

import android.content.res.Resources
import com.volkov.yabl.utils.TimeFormatter
import dagger.Module
import dagger.Provides

@Module
class UtilsModule {

    @Provides
    fun provideTimeFormatter(res: Resources) = TimeFormatter(res)

}