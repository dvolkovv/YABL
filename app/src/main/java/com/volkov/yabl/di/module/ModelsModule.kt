package com.volkov.yabl.di.module

import com.volkov.yabl.data.model.main.IMainModel
import com.volkov.yabl.data.model.main.MainModel
import com.volkov.yabl.data.model.settings.ISettingsModel
import com.volkov.yabl.data.model.settings.SettingsModel
import dagger.Binds
import dagger.Module

@Module
abstract class ModelsModule {

    @Binds
    abstract fun mainModel(mainModel: MainModel): IMainModel

    @Binds
    abstract fun settingsModel(settingsModel: SettingsModel): ISettingsModel
}