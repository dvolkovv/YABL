package com.volkov.yabl.di.module

import android.content.Context
import com.volkov.db.YablDatabase
import com.volkov.db.dao.CurrentBookDao
import com.volkov.db.dao.LastBooksDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): YablDatabase = initDatabase(context)

    @Provides
    @Singleton
    fun provideCurrentBooksDao(database: YablDatabase): CurrentBookDao = database.currentBookDao()

    @Provides
    @Singleton
    fun provideLastBooksDao(database: YablDatabase): LastBooksDao = database.lastBooksDao()

    private fun initDatabase(context: Context) = YablDatabase.init(context)
}