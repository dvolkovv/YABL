package com.volkov.yabl.di.scopes

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PlayerServiceScope