package com.volkov.yabl.view.adapter

import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.volkov.yabl.R
import com.volkov.yabl.view.data.LastBook
import kotlinx.android.synthetic.main.book_view_holder.view.*
import kotlinx.android.synthetic.main.delete_button.view.*


class LastBooksAdapter : ListAdapter<LastBook, LastBooksAdapter.LastBookVH>(callback) {
    var onItemClickListener: ((Uri) -> Unit)? = null
    var onItemDeleteClickListener: ((Uri) -> Unit)? = null

    private val binder = ViewBinderHelper().also { it.setOpenOnlyOne(true) }

    companion object {
        private val callback = object : DiffUtil.ItemCallback<LastBook>() {
            override fun areItemsTheSame(
                oldItem: LastBook,
                newItem: LastBook
            ): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(
                oldItem: LastBook,
                newItem: LastBook
            ): Boolean =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LastBookVH {
        val layout =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.book_view_holder, parent, false)
        val viewHolder = LastBookVH(layout)
        layout.book_layout.setOnClickListener {
            val book = getItemByViewHolder(viewHolder) ?: return@setOnClickListener
            onItemClick(book)
        }
        layout.delete_button.setOnClickListener {
            val book = getItemByViewHolder(viewHolder) ?: return@setOnClickListener
            onDeleteClick(book)
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: LastBookVH, position: Int) {
        val book = getItem(position)
        binder.bind(holder.itemView.root, book.filepath.toString())
        holder.bind(book)
    }

    override fun onCurrentListChanged(
        previousList: MutableList<LastBook>,
        currentList: MutableList<LastBook>
    ) {
        currentList.minus(previousList.toHashSet())
            .forEach { closeLayout(it) }
    }

    private fun closeLayout(lastBook: LastBook) {
        binder.closeLayout(lastBook.filepath.toString())
    }

    private fun getItemByViewHolder(vh: RecyclerView.ViewHolder): LastBook? {
        val pos = vh.adapterPosition
        if (pos < currentList.size && pos >= 0) {
            return getItem(pos)
        }
        return null
    }

    private fun onItemClick(book: LastBook) {
        onItemClickListener?.invoke(book.filepath)
    }

    private fun onDeleteClick(book: LastBook) {
        onItemDeleteClickListener?.invoke(book.filepath)
    }

    inner class LastBookVH(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(book: LastBook) {
            val vhContext = itemView.context
            val fallbackDrawable: Drawable? by lazy {
                ContextCompat.getDrawable(vhContext, R.drawable.empty_cover)
            }

            itemView.book_elapsed_time.text = book.elapsedTime
            itemView.book_title.text = book.title
            itemView.book_progress_percent.text =
                vhContext.getString(R.string.progress_percent, book.progressInPercent)
            Glide.with(vhContext)
                .load(book.cover)
                .placeholder(CircularProgressDrawable(vhContext))
                .fallback(fallbackDrawable)
                .into(itemView.book_cover)
        }
    }

}
