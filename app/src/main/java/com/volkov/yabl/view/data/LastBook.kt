package com.volkov.yabl.view.data

import android.net.Uri

data class LastBook(
    val title: String,
    val elapsedTime: String,
    val filepath: Uri,
    val cover: ByteArray?,
    val progressInPercent: Int
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LastBook

        if (title != other.title) return false
        if (elapsedTime != other.elapsedTime) return false
        if (filepath != other.filepath) return false
        if (progressInPercent != other.progressInPercent) return false

        return true
    }

    override fun hashCode(): Int {
        var result = title.hashCode()
        result = 31 * result + elapsedTime.hashCode()
        result = 31 * result + filepath.hashCode()
        result = 31 * result + progressInPercent
        return result
    }
}