package com.volkov.yabl.view.ui.main

import android.net.Uri
import com.volkov.yabl.view.base.BaseView
import com.volkov.yabl.view.data.CurrentBook
import com.volkov.yabl.view.data.LastBook
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

interface MainView : BaseView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onCurrentBookLoaded(currentBook: CurrentBook?)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onLastBooksLoaded(books: List<LastBook>?)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onSpeedModLoaded(speedMod: Float)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setNewPlaytime(time: Int)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showBookIsNotExistDialog(onConfirmErase: () -> Unit)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showBookIsNotExistToast()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showRevertDeleteOffer()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showPlayerError()

    @StateStrategyType(AddToEndSingleTagStrategy::class, tag = DELETE_DIALOG_TAG)
    fun showDeleteDialog(uriToDelete: Uri)

    @StateStrategyType(AddToEndSingleTagStrategy::class, tag = DELETE_DIALOG_TAG)
    fun hideDeleteDialog()

    @StateStrategyType(AddToEndSingleTagStrategy::class, tag = PLAYER_STATE_TAG)
    fun setPausedState()

    @StateStrategyType(AddToEndSingleTagStrategy::class, tag = PLAYER_STATE_TAG)
    fun setPlayingState()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun openFilePicker()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun openSettingsActivity()

    companion object {
        private const val PLAYER_STATE_TAG = "player_state"
        private const val DELETE_DIALOG_TAG = "delete_dialog"
    }

}
