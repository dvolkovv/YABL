package com.volkov.yabl.view.ui.settings

import com.volkov.yabl.YABL
import com.volkov.yabl.data.model.settings.ISettingsModel
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class SettingsPresenter : MvpPresenter<SettingsView>() {

    @Inject
    lateinit var model: ISettingsModel

    init {
        YABL.components.appComponent.inject(this)
    }

    fun loadCurrentSettings() {
        model.loadShouldDeleteWithoutAsking().let {
            viewState.onDeleteSettingLoaded(it)
        }
        model.loadNightModeSetting().let {
            viewState.onNightModeSettingLoaded(it)
        }
    }

    fun setDeleteWithoutAsking(deleteWithoutAsking: Boolean) {
        model.setShouldDeleteWithoutAsking(deleteWithoutAsking)
    }

    fun setNightMode(enabled: Boolean) {
        model.setNightMode(enabled)
    }

}