package com.volkov.yabl.view.ui.main

import android.net.Uri
import com.volkov.filemanager.BookNotFoundOnPathException
import com.volkov.yabl.YABL
import com.volkov.yabl.data.model.main.IMainModel
import com.volkov.yabl.player.BookPlayerObserver
import com.volkov.yabl.player.state.BookPlayerState
import com.volkov.yabl.player.state.PausedState
import com.volkov.yabl.player.state.PlayingState
import com.volkov.yabl.player.state.ReadyState
import moxy.InjectViewState
import moxy.MvpPresenter
import java.io.IOException
import javax.inject.Inject

@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {

    @Inject
    lateinit var model: IMainModel

    private var deleteBookCommandId: Int? = null

    init {
        YABL.components.appComponent.inject(this)
        val playerObserver = BookPlayerObserver(this::onPlaytimeChanged, this::onPlayerStateChanged)
        model.addBookPlayerObserver(playerObserver)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadAllBooks()
        loadSpeedMod()
    }

    override fun onDestroy() {
        super.onDestroy()
        model.onDestroy()
    }

    fun onBookSelected(bookUri: Uri) {
        model.setBookByUri(
            bookUri,
            onError = { onFileNotFound() }
        )
        loadAllBooks()
    }

    fun onLastBookSelected(bookUri: Uri) {
        model.setBookByUri(
            bookUri,
            onError = ::onLastBookErasedInStorage
        )
        loadAllBooks()
    }

    fun onSettingsActionClicked() {
        viewState.openSettingsActivity()
    }

    fun onOpenBookActionClicked() {
        viewState.openFilePicker()
    }

    fun onBookDeleteClicked(bookUri: Uri?) {
        bookUri ?: return
        if (model.getShouldDeleteWithoutAsking()) {
            removeBookAndReload(bookUri)
        } else {
            viewState.showDeleteDialog(bookUri)
        }
    }

    fun onDeleteDialogDismissed() {
        viewState.hideDeleteDialog()
    }

    fun onPlayClick() {
        model.playBook()
    }

    fun onPauseClick() {
        model.pauseBook()
    }

    fun onForwardClick() {
        model.forwardBookByThirty()
    }

    fun onRewindClick() {
        model.rewindBookByThirty()
    }

    fun onRememberCheckChanged(shouldRemember: Boolean) {
        model.setDeleteWithoutAsking(shouldRemember)
    }

    fun setBookTime(time: Int) {
        model.setPlaytime(time)
    }

    fun onSpeedSelected(speedMod: Float) {
        model.setPlaybackSpeed(speedMod)
    }

    fun deleteBookByDialog(uri: Uri) {
        viewState.hideDeleteDialog()
        removeBookAndReload(uri)
    }

    fun revertDelete() {
        deleteBookCommandId?.let { model.revertCommand(it) }
        loadLastBooks()
    }

    private fun loadSpeedMod() {
        model.getPlaybackSpeed().let { viewState.onSpeedModLoaded(it) }
    }

    private fun loadAllBooks() {
        loadCurrentBook()
        loadLastBooks()
    }

    private fun loadLastBooks() {
        model.loadLastBooks(
            onSuccess = { viewState.onLastBooksLoaded(it) }
        )
    }

    private fun loadCurrentBook() {
        viewState.showLoading()
        model.loadCurrentBook(
            onSuccess = {
                viewState.onCurrentBookLoaded(it)
            },
            onError = {
                onCurrentBookLoadError(it)
            }
        )
        viewState.hideLoading()
    }

    private fun onCurrentBookLoadError(err: Throwable) {
        viewState.onCurrentBookLoaded(null)
        showErrorByThrowable(err)
    }

    private fun onFileNotFound() {
        viewState.showBookIsNotExistToast()
    }

    private fun onLastBookErasedInStorage(err: Throwable) {
        showErrorByThrowable(err)
    }

    private fun showErrorByThrowable(err: Throwable) {
        when (err) {
            is BookNotFoundOnPathException -> {
                viewState.showBookIsNotExistDialog {
                    removeBookAndReload(err.bookUri)
                }
            }
            is IOException -> {
                viewState.showPlayerError()
            }
        }
    }

    private fun removeBookAndReload(uri: Uri) {
        deleteBookCommandId = model.removeBookByUri(uri)
        loadAllBooks()
        viewState.showRevertDeleteOffer()
    }

    private fun onPlaytimeChanged(newTime: Int) {
        viewState.setNewPlaytime(newTime)
    }

    private fun onPlayerStateChanged(state: BookPlayerState) {
        when (state) {
            is PlayingState -> {
                viewState.setPlayingState()
            }
            is PausedState, is ReadyState -> {
                viewState.setPausedState()
            }
            else -> {
            }
        }
    }

}