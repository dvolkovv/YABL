package com.volkov.yabl.view.base

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleTagStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

private const val LOADING_COMMAND_TAG = "performing_loading"

interface BaseView : MvpView {
    @StateStrategyType(AddToEndSingleTagStrategy::class, tag = LOADING_COMMAND_TAG)
    fun showLoading()

    @StateStrategyType(AddToEndSingleTagStrategy::class, tag = LOADING_COMMAND_TAG)
    fun hideLoading()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showUndefinedError()

}