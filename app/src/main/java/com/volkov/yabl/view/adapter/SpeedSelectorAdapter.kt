package com.volkov.yabl.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.volkov.yabl.R

class SpeedSelectorAdapter(
    private val context: Context,
    @LayoutRes private val dropdownView: Int
) : BaseAdapter() {

    private val data: ArrayList<String> = arrayListOf()

    init {
        context.resources.getStringArray(R.array.speed_modifiers).let(data::addAll)
    }

    fun getSpeedModifier(position: Int): Float? {
        return getItem(position).let(::parseFloat)
    }

    fun getPosition(mod: Float): Int {
        data.forEachIndexed { index, view ->
            if (parseFloat(view) == mod) return index
        }
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View =
            convertView ?: LayoutInflater.from(context)
                .inflate(dropdownView, parent, false)
        runCatching {
            (view as TextView).text = data[position]
        }

        return view
    }

    override fun getItem(position: Int): String = data[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = data.size

    private fun parseFloat(view: String) = view.filter { it.isDigit() || it == '.' }.toFloat()

}