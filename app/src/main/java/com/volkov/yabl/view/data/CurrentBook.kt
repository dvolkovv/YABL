package com.volkov.yabl.view.data

import android.net.Uri

data class CurrentBook(
    var playTime: Int,
    val duration: Int,
    val playTimeFormatted: String,
    val title: String,
    val durationFormatted: String,
    val filepath: Uri,
    val cover: ByteArray?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CurrentBook

        if (playTimeFormatted != other.playTimeFormatted) return false
        if (title != other.title) return false
        if (durationFormatted != other.durationFormatted) return false
        if (filepath != other.filepath) return false

        return true
    }

    override fun hashCode(): Int {
        var result = playTimeFormatted.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + durationFormatted.hashCode()
        result = 31 * result + filepath.hashCode()
        return result
    }

}