package com.volkov.yabl.view.ui.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import com.volkov.yabl.R
import kotlinx.android.synthetic.main.activity_settings.*
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter


class SettingsActivity : MvpAppCompatActivity(), SettingsView {
    private val presenter: SettingsPresenter by moxyPresenter { SettingsPresenter() }

    private val onDarkThemeCheckListener = { _: Button, isChecked: Boolean ->
        presenter.setNightMode(isChecked)
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, SettingsActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        initToolbar()
        initViews()
    }

    override fun onStart() {
        delegate.applyDayNight()
        super.onStart()
        presenter.loadCurrentSettings()
    }

    override fun onDeleteSettingLoaded(shouldDeleteWithoutAsking: Boolean) {
        should_delete_without_asking_switch?.isChecked = shouldDeleteWithoutAsking
    }

    override fun onNightModeSettingLoaded(isNightModeEnabled: Boolean) {
        night_mode_switch.setOnCheckedChangeListener(null)
        night_mode_switch.isChecked = isNightModeEnabled
        night_mode_switch.setOnCheckedChangeListener(onDarkThemeCheckListener)
    }

    private fun initViews() {
        should_delete_without_asking_switch?.setOnCheckedChangeListener { _, isChecked ->
            presenter.setDeleteWithoutAsking(isChecked)
        }
        dont_ask_click_area.setOnClickListener { should_delete_without_asking_switch.performClick() }
        dark_mode_click_area.setOnClickListener { night_mode_switch.performClick() }
        night_mode_switch?.setOnCheckedChangeListener(onDarkThemeCheckListener)
    }

    private fun initToolbar() {
        toolbar.also {
            it.setNavigationOnClickListener {
                onBackPressed()
            }
        }
    }
}