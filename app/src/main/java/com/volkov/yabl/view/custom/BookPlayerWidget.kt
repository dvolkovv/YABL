package com.volkov.yabl.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.bumptech.glide.Glide
import com.volkov.yabl.R
import com.volkov.yabl.YABL
import com.volkov.yabl.utils.TimeFormatter
import com.volkov.yabl.utils.gone
import com.volkov.yabl.utils.setOnItemSelectedListener
import com.volkov.yabl.utils.show
import com.volkov.yabl.view.adapter.SpeedSelectorAdapter
import com.volkov.yabl.view.data.CurrentBook
import kotlinx.android.synthetic.main.book_player_widget.view.*
import javax.inject.Inject

class BookPlayerWidget(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttrs: Int
) : ConstraintLayout(context, attrs, defStyleAttrs) {

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)

    var onTimeSetListener: ((time: Int) -> Unit)? = null
    var onSpeedSelectedListener: ((speedMod: Float) -> Unit) = {}

    @Inject
    lateinit var timeFormatter: TimeFormatter

    private var pauseClickListener: ((View) -> Unit)? = null
    private var playClickListener: ((View) -> Unit)? = null

    private val onProgressBarPositionChangedListener: OnSeekBarChangeListener =
        object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (!fromUser) return
                onTimeSetListener?.invoke(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        }

    init {
        YABL.components.appComponent.inject(this)
        inflate(context, R.layout.book_player_widget, this)
        initSpinner()
        book_progress_bar.setOnSeekBarChangeListener(onProgressBarPositionChangedListener)
    }

    fun setLoading(isLoading: Boolean) {
        if (isLoading) {
            book_loading_views.show()
        } else {
            book_loading_views.gone()
        }
    }

    fun setOnPlayClickListener(func: (view: View) -> Unit) {
        playClickListener = func
    }

    fun setOnPauseClickListener(func: (view: View) -> Unit) {
        pauseClickListener = func
    }

    fun setOnForwardClickListener(func: (view: View) -> Unit) {
        setButtonClick(func, button_forward)
    }

    fun setOnRewindClickListener(func: (view: View) -> Unit) {
        setButtonClick(func, button_rewind)
    }

    fun setBook(book: CurrentBook?) {
        if (book == null) {
            setUninitialisedState()
            return
        }
        setTime(book.playTimeFormatted, book.durationFormatted)
        book_progress_bar.max = book.duration
        setCurrentProgress(book.playTime)
        setTitle(book.title)
        setBookCover(book.cover)
    }

    fun setPlayingState() {
        button_play.setImageResource(R.drawable.ic_pause)
        button_play.setOnClickListener(pauseClickListener)
    }

    fun setPausedState() {
        button_play.setImageResource(R.drawable.ic_play_arrow)
        button_play.setOnClickListener(playClickListener)
    }

    fun setCurrentProgress(time: Int?) {
        if (time == null) {
            book_progress_bar?.progress = 0
        } else {
            book_progress_bar?.progress = time
            current_time_textview?.text = timeFormatter.formatBookTime(time)
        }
    }

    fun setSelectedSpeedMod(speedMod: Float) {
        val speedSelector = speed_modifier_selector ?: return
        val adapter = speedSelector.adapter as? SpeedSelectorAdapter ?: return
        speedSelector.setSelection(adapter.getPosition(speedMod), false)
    }

    private fun setUninitialisedState() {
        setTime(null, null)
        setTitle(null)
        setCurrentProgress(null)
        setBookCover(null)
    }

    private fun initSpinner() {
        val speedSelector = speed_modifier_selector ?: return
        val selectorAdapter = SpeedSelectorAdapter(
            context,
            android.R.layout.simple_spinner_dropdown_item
        )
        speedSelector.apply {
            adapter = selectorAdapter
            setSelection(selectorAdapter.getPosition(1f), false)
            setOnItemSelectedListener { position ->
                selectorAdapter.getSpeedModifier(position)
                    ?.let(onSpeedSelectedListener::invoke)
            }
        }
    }

    private fun setTime(currentTime: String?, duration: String?) {
        val fallbackString = context.getString(R.string._00_00)
        current_time_textview?.text = currentTime ?: fallbackString
        duration_text_view?.text = duration ?: fallbackString
    }

    private fun setTitle(text: String?) {
        book_title?.text = text
    }

    private fun setBookCover(bitmap: ByteArray?) {
        if (bitmap == null) {
            book_cover.gone()
            centerTitleView()
            book_cover.setImageDrawable(null)
            return
        }
        Glide.with(context).load(bitmap).into(book_cover)
        book_cover.show()
        moveTitleViewToLeft()
    }

    private fun centerTitleView() {
        val conSet = ConstraintSet()
        conSet.clone(this)
        conSet.setHorizontalBias(R.id.book_title, .5f)
        conSet.applyTo(this)
    }

    private fun moveTitleViewToLeft() {
        val conSet = ConstraintSet()
        conSet.clone(this)
        conSet.setHorizontalBias(R.id.book_title, 0f)
        conSet.applyTo(this)
    }

    private fun setButtonClick(func: (view: View) -> Unit, button: View?) {
        button?.setOnClickListener(func)
    }

}