package com.volkov.yabl.view.ui.main

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.volkov.yabl.R
import com.volkov.yabl.utils.shortToast
import com.volkov.yabl.view.adapter.LastBooksAdapter
import com.volkov.yabl.view.data.CurrentBook
import com.volkov.yabl.view.data.LastBook
import com.volkov.yabl.view.ui.settings.SettingsActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.alert_dialog.view.*
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter


private const val FILE_MANAGER_REQUEST_CODE = 172

class MainActivity : MvpAppCompatActivity(), MainView {

    private val presenter: MainPresenter by moxyPresenter { MainPresenter() }
    private val lastBooksAdapter = LastBooksAdapter()

    private var alertDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        initViews()
    }

    override fun onStart() {
        delegate.applyDayNight()
        super.onStart()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_open_book -> {
                presenter.onOpenBookActionClicked()
                true
            }
            R.id.action_settings -> {
                presenter.onSettingsActionClicked()
                true
            }
            else -> false
        }
    }

    override fun showBookIsNotExistDialog(onConfirmErase: () -> Unit) {
        Snackbar.make(
            findViewById(android.R.id.content),
            R.string.book_is_already_deleted,
            Snackbar.LENGTH_LONG
        ).setAction(R.string.yes) { onConfirmErase.invoke() }.show()
    }

    override fun showBookIsNotExistToast() {
        shortToast(R.string.media_file_input_error)
    }

    override fun showUndefinedError() {
        shortToast(R.string.unknown_error)
    }

    override fun showPlayerError() {
        shortToast(R.string.player_state_error)
    }

    override fun showRevertDeleteOffer() {
        Snackbar.make(
            findViewById(android.R.id.content),
            R.string.revert_delete,
            resources.getInteger(R.integer.snackbar_revert_lifetime)
        ).setAction(R.string.yes) { presenter.revertDelete() }.show()
    }

    @SuppressLint("InflateParams")
    override fun showDeleteDialog(uriToDelete: Uri) {
        val alertDialogView = layoutInflater.inflate(R.layout.alert_dialog, null)
        val dismissListener = { presenter.onDeleteDialogDismissed() }
        val positiveListener: (DialogInterface, Int) -> Unit =
            { _, _ ->
                presenter.deleteBookByDialog(uriToDelete)
                presenter.onRememberCheckChanged(alertDialogView.remember_check.isChecked)
            }
        alertDialog = AlertDialog.Builder(this)
            .setTitle(R.string.delete)
            .setView(alertDialogView)
            .setPositiveButton(R.string.delete, positiveListener)
            .setNegativeButton(R.string.cancel) { _, _ -> dismissListener.invoke() }
            .setOnDismissListener { dismissListener.invoke() }
            .show()
    }

    override fun hideDeleteDialog() {
        alertDialog?.hide()
    }

    override fun showLoading() {
        bookPlayer.setLoading(true)
    }

    override fun hideLoading() {
        bookPlayer.setLoading(false)
    }

    override fun onLastBooksLoaded(books: List<LastBook>?) {
        lastBooksAdapter.submitList(books)
    }

    override fun onCurrentBookLoaded(currentBook: CurrentBook?) {
        bookPlayer.setBook(currentBook)
    }

    override fun onSpeedModLoaded(speedMod: Float) {
        bookPlayer.setSelectedSpeedMod(speedMod)
    }

    override fun setPausedState() {
        bookPlayer.setPausedState()
    }

    override fun setPlayingState() {
        bookPlayer.setPlayingState()
    }

    override fun setNewPlaytime(time: Int) {
        bookPlayer.setCurrentProgress(time)
    }

    override fun openSettingsActivity() {
        SettingsActivity.start(this)
    }

    override fun openFilePicker() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = "audio/*"
        //flags to get book with permissions to open it after device reboot/app reboot
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
        startActivityForResult(
            Intent.createChooser(intent, getString(R.string.open_book)),
            FILE_MANAGER_REQUEST_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            FILE_MANAGER_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.let {
                        //secure persistent access after we ask for permission in openFileHelper
                        contentResolver.takePersistableUriPermission(
                            it,
                            Intent.FLAG_GRANT_READ_URI_PERMISSION
                        )
                        presenter.onBookSelected(it)
                    }
                }
            }
            else -> return
        }
    }

    private fun initViews() {
        bookPlayer.apply {
            onTimeSetListener = { time: Int ->
                presenter.setBookTime(time)
            }
            onSpeedSelectedListener = { speedMod ->
                presenter.onSpeedSelected(speedMod)
            }
            setOnPlayClickListener { presenter.onPlayClick() }
            setOnPauseClickListener { presenter.onPauseClick() }
            setOnForwardClickListener { presenter.onForwardClick() }
            setOnRewindClickListener { presenter.onRewindClick() }
        }
        initRecycler()
    }

    private fun initRecycler() {
        last_books_list.let {
            it.adapter = lastBooksAdapter.also { adapter ->
                adapter.onItemClickListener = presenter::onLastBookSelected
                adapter.onItemDeleteClickListener = presenter::onBookDeleteClicked
            }
            it.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        }
    }

}
