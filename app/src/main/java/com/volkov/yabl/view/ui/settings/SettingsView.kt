package com.volkov.yabl.view.ui.settings

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface SettingsView : MvpView {
    fun onDeleteSettingLoaded(shouldDeleteWithoutAsking: Boolean)
    fun onNightModeSettingLoaded(isNightModeEnabled: Boolean)
}