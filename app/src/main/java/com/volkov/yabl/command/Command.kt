package com.volkov.yabl.command

import java.util.*
import kotlin.concurrent.schedule

abstract class Command {
    var onCommandDead: (() -> Unit)? = null

    var isCommandAlive = true
        private set

    open suspend fun revert() {
        if (!isCommandAlive) {
            return
        }
    }

    open suspend fun execute() {
        Timer().schedule(COMMAND_LIFETIME) {
            isCommandAlive = false
            onCommandDead?.invoke()
        }
    }

    companion object {
        const val COMMAND_LIFETIME: Long = 6000
    }
}