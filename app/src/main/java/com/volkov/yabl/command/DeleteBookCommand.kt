package com.volkov.yabl.command

import android.net.Uri
import com.volkov.db.entity.LastBookEntity
import com.volkov.yabl.data.repository.IBookRepository

class DeleteBookCommand(private val bookRepository: IBookRepository, private val uri: Uri) :
    Command() {

    private var deletedBook: LastBookEntity? = null

    override suspend fun execute() {
        deletedBook = bookRepository.getLastBook(uri)
        bookRepository.removeFromLastBooks(uri)
    }

    override suspend fun revert() {
        deletedBook?.let { bookRepository.addLastBook(it) }
    }
}