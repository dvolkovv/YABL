package com.volkov.yabl.utils

import android.content.res.Resources
import com.volkov.db.entity.BookEntity
import com.volkov.yabl.R
import java.text.SimpleDateFormat
import java.util.*

private const val MILLISECONDS_IN_HOUR = 36_000_00

class TimeFormatter(private val resources: Resources) {

    fun getProgressTimeString(playtime: Int, duration: Int): String {
        val timeString = formatBookTime(playtime)
        val durationString = formatBookTime(duration)
        return resources.getString(R.string.elapsed_time, timeString, durationString)
    }

    fun getProgressTimeString(book: BookEntity): String {
        return getProgressTimeString(book.playTime, book.duration)
    }

    fun formatBookTime(time: Int): String {
        require(time >= 0) { "Time is below zero! Time: $time" }
        val afterHoursPart = SimpleDateFormat("mm:ss", Locale.getDefault()).format(time)
        val bookTimeBuilder = StringBuilder()
        bookTimeBuilder.let {
            if (time.getHours() > 0) {
                it.append(time.getHours()).append(':')
            }
            it.append(afterHoursPart)
        }
        return bookTimeBuilder.toString()
    }

    private fun Int.getHours(): Int {
        return this / MILLISECONDS_IN_HOUR
    }

}