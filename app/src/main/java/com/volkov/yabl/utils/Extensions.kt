package com.volkov.yabl.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

//Context extensions
fun Context.shortToast(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun Context.shortToast(@StringRes message: Int) = shortToast(getString(message))

//View extensions
fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun View.setVisible(isVisible: Boolean) {
    if (isVisible) {
        show()
    } else {
        gone()
    }
}

//Activity extensions
fun Activity.acquirePermissions(
    requestCode: Int,
    vararg permissions: String
) {
    val nonGrantedPermissions = permissions.filter {
        isPermissionsGranted(it).not()
    }.toTypedArray()
    if (nonGrantedPermissions.isEmpty()) {
        return
    }

    ActivityCompat.requestPermissions(
        this,
        nonGrantedPermissions,
        requestCode
    )
}

fun Activity.isPermissionsGranted(vararg permissions: String): Boolean {
    return permissions.all {
        ContextCompat.checkSelfPermission(
            this,
            it
        ) == PackageManager.PERMISSION_GRANTED
    }
}

@Suppress("DEPRECATION")
fun String.fromHtml(): Spanned {
    return if (isApiMoreThan24()) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(this)
    }
}

fun Spinner.setOnItemSelectedListener(listener: (position: Int) -> Unit) {
    onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            listener.invoke(position)
        }

    }
}

fun isApiMoreThan26() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
fun isApiMoreThan24() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
fun isApiMoreThan23() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
