package com.volkov.yabl.settings

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import javax.inject.Inject

class SettingsManager @Inject constructor(context: Context) {
    companion object {
        private const val SETTINGS_PREFS_NAME = "settings_prefs"
        private const val SHOULD_DELETE_KEY = "should_delete_without_asking"
        private const val NIGHT_MODE_KEY = "night_mode_enabled"
    }

    private val prefs = context.getSharedPreferences(SETTINGS_PREFS_NAME, Context.MODE_PRIVATE)

    fun loadShouldDeleteWithoutAsking(): Boolean {
        return prefs.getBoolean(SHOULD_DELETE_KEY, false)
    }

    fun setShouldDeleteWithoutAsking(shouldDelete: Boolean) {
        prefs.edit().putBoolean(SHOULD_DELETE_KEY, shouldDelete).apply()
    }

    fun setSavedNightMode() {
        val isEnabled = getNightMode()
        setNightMode(isEnabled)
    }

    fun setNightMode(enabled: Boolean) {
        saveNightModePrefs(enabled)
        val nightMode: Int = if (enabled) {
            AppCompatDelegate.MODE_NIGHT_YES
        } else {
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
        }
        AppCompatDelegate.setDefaultNightMode(nightMode)
    }

    fun getNightMode(): Boolean {
        return prefs.getBoolean(NIGHT_MODE_KEY, false)
    }

    private fun saveNightModePrefs(isNightMode: Boolean) {
        prefs.edit()
            .putBoolean(NIGHT_MODE_KEY, isNightMode)
            .apply()
    }
}