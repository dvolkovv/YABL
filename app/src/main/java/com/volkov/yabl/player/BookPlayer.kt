package com.volkov.yabl.player

import android.content.Context
import android.media.MediaPlayer
import android.media.PlaybackParams
import android.os.Handler
import com.volkov.db.entity.BookEntity
import com.volkov.yabl.player.state.*
import com.volkov.yabl.utils.isApiMoreThan23
import java.io.IOException
import javax.inject.Inject
import kotlin.reflect.KClass

private const val THIRTY_SEC = 30000
private const val ONE_SEC: Long = 1000

class BookPlayer @Inject constructor(
    private val appContext: Context
) : MediaPlayer(), IBookPlayer,
    MediaPlayer.OnCompletionListener,
    MediaPlayer.OnErrorListener {

    private var state: BookPlayerState = NotInitializedState(this)

    private val observers = mutableSetOf<BookPlayerObserver>()
    private var currentBook: BookEntity? = null
    private val refreshHandler by lazy { Handler() }

    init {
        setOnErrorListener(this)
        setOnCompletionListener(this)
    }

    @Throws(IOException::class)
    override fun changeBook(book: BookEntity?) {
        currentBook = when (book) {
            currentBook -> return
            null -> {
                reset()
                return
            }
            else -> {
                reset()
                book
            }
        }
        setDataSource(appContext, currentBook!!.filepath)
        prepare()
        setTime(book.playTime)
        notifyBookObservers()
    }

    fun setState(state: KClass<out BookPlayerState>) {
        this.state = when (state) {
            NotInitializedState::class -> NotInitializedState(this)
            ReadyState::class -> ReadyState(this)
            PlayingState::class -> PlayingState(this)
            PausedState::class -> PausedState(this)
            else -> NotInitializedState(this)
        }
        onPlayerStateChanged(this.state)
    }

    override fun setPlaybackSpeedMod(mod: Float) {
        state.setSpeed(mod)
    }

    override fun getPlaybackSpeedMod(): Float {
        return state.speedMod
    }

    override fun play() {
        state.play()
    }

    override fun pause() {
        state.pause()
    }

    override fun reset() {
        super.reset()
        setState(NotInitializedState::class)
    }

    override fun prepare() {
        super.prepare()
        setState(ReadyState::class)
    }

    override fun forward30() {
        state.forward30()
    }

    override fun rewind30() {
        state.rewind30()
    }

    override fun setTime(time: Int) {
        state.setTime(time)

    }

    override fun addObserver(obs: BookPlayerObserver) {
        obs.notifyAll(getCurrentTime(), state, currentBook)
        observers.add(obs)
    }

    override fun onCompletion(mp: MediaPlayer?) {
        setState(ReadyState::class)
    }

    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        mp?.reset()
        return true
    }

    override fun isSet(): Boolean {
        return currentBook != null
    }

    override fun isPlaying(): Boolean {
        return state is PlayingState
    }

    override fun getCurrentTime(): Int {
        return if (currentPosition < 0) 0 else currentPosition
    }

    override fun getCurrentBookName(): String? = currentBook?.title

    override fun getBookDuration() = currentBook?.duration ?: 0

    fun startPlayback() {
        start()
    }

    fun pausePlayback() {
        super.pause()
    }

    fun performForward30() {
        val fwdPos = currentPosition + THIRTY_SEC
        setTime(fwdPos)
    }

    fun performRewind30() {
        val rwdPos = currentPosition - THIRTY_SEC
        setTime(rwdPos)
    }

    fun applySetTime(time: Int) {
        val newPosition: Int = when {
            time > duration -> duration
            time < 0 -> 0
            else -> time
        }
        seekTo(newPosition)
        notifyPlaytimeObservers()
    }

    fun applySpeedModifier(speedMod: Float) {
        runCatching {
            if (!isApiMoreThan23()) return
            if (playbackParams.speed == speedMod) return
            val newParams = PlaybackParams().allowDefaults().also { it.speed = speedMod }
            this.playbackParams = newParams
        }
    }

    private fun onPlayerStateChanged(newState: BookPlayerState) {
        when (newState) {
            is PlayingState -> {
                onPlaying()
            }
            is PausedState -> {
                onPaused()
            }
            else -> {
            }
        }
        notifyStateObservers()
    }

    private fun notifyBookObservers() {
        observers.forEach {
            it.notifyBookChanged(currentBook)
        }
    }

    private fun notifyStateObservers() {
        observers.forEach {
            it.notifyStateChanged(state)
        }
    }

    private fun notifyPlaytimeObservers() {
        observers.forEach {
            it.notifyPlaytimeChanged(currentPosition)
        }
    }

    private fun onPlaying() {
        if (hasPlaytimeObservers().not()) {
            return
        }
        val delay = ONE_SEC
        val refresh = object : Runnable {
            override fun run() {
                notifyPlaytimeObservers()
                refreshHandler.postDelayed(this, delay)
            }
        }
        refreshHandler.postDelayed(refresh, delay)
    }

    private fun onPaused() {
        refreshHandler.removeCallbacksAndMessages(null)
    }

    private fun hasPlaytimeObservers(): Boolean = observers.size > 0
}
