package com.volkov.yabl.player

import com.volkov.db.entity.BookEntity
import com.volkov.yabl.player.state.BookPlayerState

class BookPlayerObserver(
    private val playtimeObs: ((Int) -> Unit)? = null,
    private val stateObs: ((BookPlayerState) -> Unit)? = null,
    private val bookObs: ((BookEntity?) -> Unit)? = null
) {

    fun notifyAll(playtime: Int, state: BookPlayerState, book: BookEntity?) {
        notifyPlaytimeChanged(playtime)
        notifyStateChanged(state)
        notifyBookChanged(book)
    }

    fun notifyPlaytimeChanged(playtime: Int) {
        playtimeObs?.invoke(playtime)
    }

    fun notifyStateChanged(state: BookPlayerState) {
        stateObs?.invoke(state)
    }

    fun notifyBookChanged(book: BookEntity?) {
        bookObs?.invoke(book)
    }
}