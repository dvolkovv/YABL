package com.volkov.yabl.player.state

import com.volkov.yabl.player.BookPlayer

abstract class BookPlayerState(protected val player: BookPlayer) {

    var speedMod: Float = 1f
        protected set

    abstract fun play()

    abstract fun pause()

    abstract fun forward30()

    abstract fun rewind30()

    abstract fun setTime(time: Int)

    abstract fun setSpeed(mod: Float)

}