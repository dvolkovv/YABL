package com.volkov.yabl.player.state

import com.volkov.yabl.player.BookPlayer

/**
 * Any actions with player are blocked in this state
 */
class NotInitializedState(player: BookPlayer) : BookPlayerState(player) {

    override fun play() {
    }

    override fun pause() {
    }

    override fun forward30() {
    }

    override fun rewind30() {
    }

    override fun setTime(time: Int) {
    }

    override fun setSpeed(mod: Float) {
    }

}