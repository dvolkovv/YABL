package com.volkov.yabl.player.state

import com.volkov.yabl.player.BookPlayer

/**
 * Allowed to do any action with player, except play.
 * Speed mod will be applied on set.
 */
class PlayingState(player: BookPlayer) : InitializedState(player) {

    override fun setSpeed(mod: Float) {
        super.setSpeed(mod)
        player.applySpeedModifier(mod)
    }

    override fun play() {
    }
}