package com.volkov.yabl.player

import com.volkov.db.entity.BookEntity

interface IBookPlayer {

    fun changeBook(book: BookEntity?)

    fun play()
    fun pause()

    fun forward30()
    fun rewind30()

    fun setTime(time: Int)
    fun setPlaybackSpeedMod(mod: Float)
    fun getPlaybackSpeedMod(): Float

    fun isPlaying(): Boolean
    fun isSet(): Boolean
    fun getCurrentTime(): Int
    fun getCurrentBookName(): String?
    fun getBookDuration(): Int

    fun addObserver(obs: BookPlayerObserver)
}