package com.volkov.yabl.player.state

import com.volkov.yabl.player.BookPlayer

abstract class InitializedState(player: BookPlayer) : BookPlayerState(player) {

    override fun play() {
        player.startPlayback()
        player.applySpeedModifier(speedMod)
        player.setState(PlayingState::class)
    }

    override fun pause() {
        player.pausePlayback()
        player.setState(PausedState::class)
    }

    override fun forward30() {
        player.performForward30()
    }

    override fun rewind30() {
        player.performRewind30()
    }

    override fun setTime(time: Int) {
        player.applySetTime(time)
    }

    override fun setSpeed(mod: Float) {
        speedMod = mod
    }

}