package com.volkov.yabl.player.state

import com.volkov.yabl.player.BookPlayer

/**
 * Allowed to do any action with player, except pause
 */
class PausedState(player: BookPlayer) : InitializedState(player) {

    override fun pause() {
    }

}