package com.volkov.yabl.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import com.volkov.db.entity.BookEntity
import com.volkov.yabl.YABL
import com.volkov.yabl.data.model.main.IMainModel
import com.volkov.yabl.notification.NOTIFICATION_ID
import com.volkov.yabl.notification.PlayerServiceNotificationHelper
import com.volkov.yabl.player.BookPlayerObserver
import com.volkov.yabl.player.state.BookPlayerState
import com.volkov.yabl.utils.isApiMoreThan26
import javax.inject.Inject

class PlayerService : Service(), IPlayerService {

    @Inject
    lateinit var model: IMainModel
    @Inject
    lateinit var notificationHelper: PlayerServiceNotificationHelper

    private val localBinder = LocalBinder()

    companion object {
        fun startService(context: Context) {
            val serviceIntent =
                Intent(context, PlayerService::class.java)
            context.startForegroundServiceCompat(serviceIntent)
        }
    }

    init {
        YABL.components.addServiceComponent(this, this)?.inject(this)
    }

    override fun onCreate() {
        super.onCreate()
        val notification = notificationHelper.buildServiceNotification()
        val playerObserver = BookPlayerObserver(
            this::onPlaytimeChanged,
            this::onPlayerStateChanged,
            this::updateNotificationBookInfo
        )

        model.addBookPlayerObserver(playerObserver)
        startForeground(NOTIFICATION_ID, notification)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        if (intent != null && notificationHelper.onIntentReceived(intent)) {
            return START_STICKY
        }
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return localBinder
    }

    override fun onDestroy() {
        super.onDestroy()
        YABL.components.clearServiceComponent()
    }

    override fun play() {
        model.playBook()
    }

    override fun pause() {
        model.pauseBook()
    }

    override fun forward() {
        model.forwardBookByThirty()
    }

    override fun rewind() {
        model.rewindBookByThirty()
    }

    override fun close() {
        model.pauseBook()
        stopSelf()
    }

    private fun onPlaytimeChanged(newTime: Int) {
        notificationHelper.setTime(newTime)
    }

    private fun onPlayerStateChanged(state: BookPlayerState) {
        notificationHelper.onPlayerStateChanged(state)
    }

    private fun updateNotificationBookInfo(book: BookEntity?) {
        val bookTitle = book?.title ?: ""
        val bookDuration = book?.duration ?: 0

        notificationHelper.setBookTitle(bookTitle)
        notificationHelper.bookDuration = bookDuration
    }

    inner class LocalBinder : Binder() {
        fun getService(): IPlayerService = this@PlayerService
    }

}

private fun Context.startForegroundServiceCompat(service: Intent) {
    if (isApiMoreThan26()) {
        startForegroundService(service)
    } else {
        startService(service)
    }
}
