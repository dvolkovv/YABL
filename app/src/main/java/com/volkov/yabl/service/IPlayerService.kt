package com.volkov.yabl.service

interface IPlayerService {

    fun play()
    fun pause()
    fun forward()
    fun rewind()
    fun close()

}