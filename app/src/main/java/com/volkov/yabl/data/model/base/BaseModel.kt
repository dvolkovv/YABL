package com.volkov.yabl.data.model.base

import kotlinx.coroutines.*
import java.util.*

abstract class BaseModel : CoroutineScope by CoroutineScope(Dispatchers.Default) {

    private val importantJobs = LinkedList<Job>()

    protected fun <Data> launchBackgroundImportant(
        action: suspend () -> Data?,
        onComplete: ((Data?) -> Unit)? = null,
        onError: ((Throwable) -> Unit)? = null
    ) {
        launch(Dispatchers.Main) {
            try {
                withContext(coroutineContext) {
                    action.invoke()
                }.let { onComplete?.invoke(it) }
            } catch (err: Throwable) {
                err.printStackTrace()
                onError?.invoke(err)
            }
        }.let { importantJobs.add(it) }
    }

    protected fun <Data> launchBackground(
        action: suspend () -> Data?,
        onComplete: ((Data?) -> Unit)? = null,
        onError: ((Throwable) -> Unit)? = null
    ): Job {
        return launch(Dispatchers.Main) {
            try {
                withContext(coroutineContext) {
                    importantJobs.joinAll()
                    action.invoke()
                }.let { onComplete?.invoke(it) }
            } catch (err: Throwable) {
                err.printStackTrace()
                onError?.invoke(err)
            }
        }
    }
}

