package com.volkov.yabl.data.model.main

import android.net.Uri
import com.volkov.db.entity.BookEntity
import com.volkov.yabl.player.BookPlayerObserver
import com.volkov.yabl.view.data.CurrentBook
import com.volkov.yabl.view.data.LastBook
import javax.inject.Singleton

@Singleton
interface IMainModel {
    fun playBook()

    fun pauseBook()

    fun forwardBookByThirty()

    fun rewindBookByThirty()

    fun setPlaybackSpeed(speed: Float)

    fun setPlaytime(time: Int)

    fun getCurrentBookName(): String?

    fun getBookDuration(): Int

    fun addBookPlayerObserver(observer: BookPlayerObserver)

    fun loadCurrentBook(
        onSuccess: (CurrentBook?) -> Unit,
        onError: (Throwable) -> Unit
    )

    fun loadLastBooks(
        onSuccess: (List<LastBook>?) -> Unit
    )

    fun loadBookCover(
        book: BookEntity?,
        onSuccess: (ByteArray?) -> Unit
    )

    fun revertCommand(commandId: Int)

    fun setBookByUri(uri: Uri, onError: (Throwable) -> Unit)
    fun removeBookByUri(uri: Uri): Int

    fun setDeleteWithoutAsking(shouldDelete: Boolean)
    fun getShouldDeleteWithoutAsking(): Boolean

    fun onDestroy()
    fun getPlaybackSpeed(): Float
}