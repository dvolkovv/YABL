package com.volkov.yabl.data.model.main

import android.content.Context
import android.net.Uri
import android.util.SparseArray
import androidx.core.util.remove
import com.volkov.db.entity.BookEntity
import com.volkov.filemanager.BookNotFoundOnPathException
import com.volkov.filemanager.FileManager
import com.volkov.toCurrentBookEntity
import com.volkov.toLastBookEntity
import com.volkov.yabl.command.Command
import com.volkov.yabl.command.DeleteBookCommand
import com.volkov.yabl.data.mapper.BookMapper
import com.volkov.yabl.data.model.base.BaseModel
import com.volkov.yabl.data.repository.IBookRepository
import com.volkov.yabl.player.BookPlayerObserver
import com.volkov.yabl.player.IBookPlayer
import com.volkov.yabl.service.PlayerService
import com.volkov.yabl.settings.SettingsManager
import com.volkov.yabl.view.data.CurrentBook
import com.volkov.yabl.view.data.LastBook
import javax.inject.Inject

class MainModel @Inject constructor(
    private val context: Context,
    private val bookRepository: IBookRepository,
    private val player: IBookPlayer,
    private val settingsManager: SettingsManager,
    private val fileManager: FileManager,
    private val booksMapper: BookMapper
) : BaseModel(), IMainModel {

    private val commands: SparseArray<Command> = SparseArray()

    override fun playBook() {
        player.play()
        PlayerService.startService(context)
    }

    override fun pauseBook() {
        player.pause()
    }

    override fun forwardBookByThirty() {
        player.forward30()
    }

    override fun rewindBookByThirty() {
        player.rewind30()
    }

    override fun setBookByUri(uri: Uri, onError: ((Throwable) -> Unit)) {
        updateTime()
        launchBackgroundImportant(
            {
                val bookFromStorage = bookRepository.getBookFromStorage(uri)
                    ?: throw BookNotFoundOnPathException(uri)
                val currentBook = bookRepository.getCurrentBook()
                if (currentBook?.filepath == uri) {
                    return@launchBackgroundImportant
                }
                if (currentBook != null) {
                    bookRepository.pushToLastBooks(currentBook.toLastBookEntity())
                }
                val lastBook = bookRepository.getLastBook(uri)
                if (lastBook != null) {
                    bookRepository.removeFromLastBooks(lastBook)
                    bookRepository.setCurrentBook(lastBook.toCurrentBookEntity())
                } else {
                    bookRepository.setCurrentBook(bookFromStorage)
                }
            },
            onError = onError
        )
    }

    override fun revertCommand(commandId: Int) {
        launchBackgroundImportant({
            commands.get(commandId)?.revert()
            commands.remove(commandId)
        })
    }

    override fun getPlaybackSpeed(): Float {
        return player.getPlaybackSpeedMod()
    }

    override fun setPlaybackSpeed(speed: Float) {
        player.setPlaybackSpeedMod(speed)
    }

    override fun setPlaytime(time: Int) {
        player.setTime(time)
    }

    override fun addBookPlayerObserver(observer: BookPlayerObserver) {
        player.addObserver(observer)
    }

    override fun loadCurrentBook(
        onSuccess: (CurrentBook?) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        launchBackground(
            {
                val entity = bookRepository.getCurrentBook()
                player.changeBook(entity)
                entity?.let { booksMapper.convertToCurrentBook(it) }
            },
            onSuccess,
            {
                removeCurrentBook()
                onError.invoke(it)
            }
        )
    }

    override fun loadBookCover(
        book: BookEntity?,
        onSuccess: (ByteArray?) -> Unit
    ) {
        if (book == null) {
            onSuccess.invoke(null)
            return
        }
        launchBackground(
            { fileManager.getBookCoverBytes(book.filepath) },
            onSuccess
        )
    }

    override fun loadLastBooks(
        onSuccess: (List<LastBook>?) -> Unit
    ) {
        launchBackground(
            {
                bookRepository.getAllLastBooks()
                    .map { booksMapper.convertToLastBook(it) }
            },
            onSuccess
        )
        return
    }

    override fun getCurrentBookName(): String? {
        return player.getCurrentBookName()
    }

    override fun getBookDuration(): Int {
        return player.getBookDuration()
    }

    override fun setDeleteWithoutAsking(shouldDelete: Boolean) {
        settingsManager.setShouldDeleteWithoutAsking(shouldDelete)
    }

    override fun getShouldDeleteWithoutAsking(): Boolean {
        return settingsManager.loadShouldDeleteWithoutAsking()
    }

    override fun onDestroy() {
        updateTime()
    }

    override fun removeBookByUri(uri: Uri): Int {
        val delete = DeleteBookCommand(bookRepository, uri)
        launchBackgroundImportant({
            delete.execute()
        })
        commands.size().let { id ->
            delete.onCommandDead = { commands.remove(id, delete) }
            commands.append(id, delete)
            return id
        }
    }

    private fun removeCurrentBook() {
        launchBackgroundImportant({
            bookRepository.removeCurrentBook()
        })
    }

    private fun updateTime() {
        launchBackgroundImportant(
            { bookRepository.updateCurrentTime(player.getCurrentTime()) }
        )
    }
}
