package com.volkov.yabl.data.model.settings

interface ISettingsModel {

    fun loadShouldDeleteWithoutAsking(): Boolean

    fun loadNightModeSetting(): Boolean

    fun setShouldDeleteWithoutAsking(shouldDelete: Boolean)

    fun setNightMode(enabled: Boolean)

}