package com.volkov.yabl.data.model.settings

import com.volkov.yabl.settings.SettingsManager
import javax.inject.Inject

class SettingsModel @Inject constructor(
    private val settingsManager: SettingsManager
) : ISettingsModel {

    override fun loadShouldDeleteWithoutAsking(): Boolean {
        return settingsManager.loadShouldDeleteWithoutAsking()
    }

    override fun loadNightModeSetting(): Boolean {
        return settingsManager.getNightMode()
    }

    override fun setShouldDeleteWithoutAsking(shouldDelete: Boolean) {
        settingsManager.setShouldDeleteWithoutAsking(shouldDelete)
    }

    override fun setNightMode(enabled: Boolean) {
        settingsManager.setNightMode(enabled)
    }

}