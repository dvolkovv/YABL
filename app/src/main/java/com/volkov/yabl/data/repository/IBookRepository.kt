package com.volkov.yabl.data.repository

import android.net.Uri
import com.volkov.db.entity.CurrentBookEntity
import com.volkov.db.entity.LastBookEntity
import javax.inject.Singleton

@Singleton
interface IBookRepository {
    suspend fun setCurrentBook(book: CurrentBookEntity)
    suspend fun addLastBook(book: LastBookEntity)

    suspend fun pushToLastBooks(book: LastBookEntity)

    suspend fun getLastBook(uri: Uri): LastBookEntity?

    suspend fun getAllLastBooks(): List<LastBookEntity>

    suspend fun getCurrentBook(): CurrentBookEntity?

    suspend fun getBookFromStorage(uri: Uri): CurrentBookEntity?

    suspend fun removeCurrentBook()
    suspend fun removeFromLastBooks(bookUri: Uri)
    suspend fun removeFromLastBooks(book: LastBookEntity)

    suspend fun updateCurrentTime(time: Int)
}
