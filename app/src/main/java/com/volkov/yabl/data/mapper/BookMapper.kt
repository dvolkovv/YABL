package com.volkov.yabl.data.mapper

import com.volkov.db.entity.CurrentBookEntity
import com.volkov.db.entity.LastBookEntity
import com.volkov.filemanager.FileManager
import com.volkov.yabl.utils.TimeFormatter
import com.volkov.yabl.view.data.CurrentBook
import com.volkov.yabl.view.data.LastBook
import javax.inject.Inject


class BookMapper @Inject constructor(
    private val timeFormatter: TimeFormatter,
    private val fileManager: FileManager
) {

    fun convertToCurrentBook(entity: CurrentBookEntity): CurrentBook {
        val cover = fileManager.getBookCoverBytes(entity.filepath)
        val formattedTime = timeFormatter.formatBookTime(entity.playTime)
        val formattedDuration = timeFormatter.formatBookTime(entity.duration)
        return CurrentBook(
            entity.playTime,
            entity.duration,
            formattedTime,
            entity.title,
            formattedDuration,
            entity.filepath,
            cover
        )
    }

    fun convertToLastBook(entity: LastBookEntity): LastBook {
        val elapsedTime = timeFormatter.getProgressTimeString(entity)
        val cover = fileManager.getBookCoverBytes(entity.filepath)
        return LastBook(
            entity.title,
            elapsedTime,
            entity.filepath,
            cover,
            entity.getProgressInPercent()
        )
    }

}