package com.volkov.yabl.data.repository


import android.net.Uri
import com.volkov.db.dao.CurrentBookDao
import com.volkov.db.dao.LastBooksDao
import com.volkov.db.entity.CurrentBookEntity
import com.volkov.db.entity.LastBookEntity
import com.volkov.filemanager.FileManager
import com.volkov.toCurrentBookEntity
import javax.inject.Inject

class BookRepository @Inject constructor(
    private val lastBooksDao: LastBooksDao,
    private val currentBookDao: CurrentBookDao,
    private val fileManager: FileManager
) : IBookRepository {
    override suspend fun getAllLastBooks(): List<LastBookEntity> {
        return lastBooksDao.getAll()
    }

    override suspend fun getCurrentBook(): CurrentBookEntity? {
        return currentBookDao.getCurrentBook()
    }

    override suspend fun setCurrentBook(book: CurrentBookEntity) {
        currentBookDao.insert(book)
    }

    override suspend fun addLastBook(book: LastBookEntity) {
        lastBooksDao.insert(book)
    }

    override suspend fun updateCurrentTime(time: Int) {
        currentBookDao.updateTime(time)
    }

    override suspend fun removeCurrentBook() {
        currentBookDao.delete()
    }

    override suspend fun removeFromLastBooks(book: LastBookEntity) {
        lastBooksDao.delete(book)
    }

    override suspend fun removeFromLastBooks(bookUri: Uri) {
        lastBooksDao.delete(bookUri)
    }

    override suspend fun getBookFromStorage(uri: Uri): CurrentBookEntity? {
        return runCatching {
            fileManager.getFileByUri(uri).toCurrentBookEntity()
        }.getOrNull()
    }

    override suspend fun getLastBook(uri: Uri): LastBookEntity? {
        return lastBooksDao.getLastBook(uri)
    }

    override suspend fun pushToLastBooks(book: LastBookEntity) {
        lastBooksDao.insert(book)
    }
}