package com.volkov.db.dao

import android.net.Uri
import androidx.room.*
import com.volkov.db.entity.LastBookEntity

@Dao
interface LastBooksDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(book: LastBookEntity)

    @Delete
    suspend fun delete(book: LastBookEntity)

    @Query("Delete from last_books where filepath=:filepath")
    suspend fun delete(filepath: Uri)

    @Update
    suspend fun update(book: LastBookEntity)

    @Query("Select * from last_books order by lastTimePlayed desc")
    suspend fun getAll(): List<LastBookEntity>

    @Query("Select * from last_books where filepath = :filepath")
    suspend fun getLastBook(filepath: Uri): LastBookEntity?

}