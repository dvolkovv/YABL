package com.volkov.db.dao

import androidx.room.*
import com.volkov.db.entity.CurrentBookEntity

@Dao
interface CurrentBookDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(book: CurrentBookEntity)

    @Update
    suspend fun update(book: CurrentBookEntity)

    @Query("Delete from current_books")
    suspend fun delete()

    @Query("Update current_books Set playTime = :time")
    suspend fun updateTime(time: Int)

    @Query("SELECT * FROM current_books")
    suspend fun getCurrentBook(): CurrentBookEntity?

    @Query("SELECT * FROM current_books")
    fun getCurrentBookSync(): CurrentBookEntity?
}