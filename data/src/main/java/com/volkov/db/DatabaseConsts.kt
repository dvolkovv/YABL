package com.volkov.db

object DatabaseConsts {
    const val DATABASE_NAME = "YABL-database"
    const val DATABASE_VER = 1
    const val LAST_BOOK_TABLE_NAME = "last_books"
    const val CURRENT_BOOK_TABLE_NAME = "current_books"
}
