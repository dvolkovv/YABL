package com.volkov.db.entity

import android.net.Uri
import androidx.room.Ignore

abstract class BookEntity(
    @Ignore
    open var playTime: Int,
    @Ignore
    open val title: String,
    @Ignore
    open val duration: Int,
    @Ignore
    open val filepath: Uri
) {

    fun getProgressInPercent(): Int {
        return playTime * 100 / duration
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BookEntity

        if (title != other.title) return false
        if (duration != other.duration) return false
        if (filepath != other.filepath) return false

        return true
    }

    override fun hashCode(): Int {
        var result = title.hashCode()
        result = 31 * result + duration
        result = 31 * result + filepath.hashCode()
        return result
    }


}