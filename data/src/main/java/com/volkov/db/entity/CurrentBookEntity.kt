package com.volkov.db.entity

import android.net.Uri
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.volkov.db.DatabaseConsts

@Entity(tableName = DatabaseConsts.CURRENT_BOOK_TABLE_NAME)
class CurrentBookEntity(
    @PrimaryKey(autoGenerate = false) val id: Int = 0,
    override var playTime: Int = 0,
    override val title: String,
    override val duration: Int,
    override val filepath: Uri
) : BookEntity(playTime, title, duration, filepath)