package com.volkov.db.entity

import android.net.Uri
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.volkov.db.DatabaseConsts
import java.util.*

@Entity(tableName = DatabaseConsts.LAST_BOOK_TABLE_NAME)
class LastBookEntity(
    override var playTime: Int = 0,
    override val title: String,
    override val duration: Int,
    val lastTimePlayed: Date,
    @PrimaryKey override val filepath: Uri
) : BookEntity(playTime, title, duration, filepath)