package com.volkov.db.converters

import android.net.Uri
import androidx.room.TypeConverter
import java.util.*

class DatabaseTypesConverter {

    @TypeConverter
    fun Uri.toStringConverter() = this.toString()

    @TypeConverter
    fun String.toUriConverter(): Uri {
        return Uri.parse(this)
    }

    @TypeConverter
    fun Date.toLongConverter() = this.time

    @TypeConverter
    fun Long.toDateConverter() = Date(this)


}