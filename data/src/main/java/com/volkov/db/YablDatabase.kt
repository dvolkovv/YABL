package com.volkov.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.volkov.db.converters.DatabaseTypesConverter
import com.volkov.db.dao.CurrentBookDao
import com.volkov.db.dao.LastBooksDao
import com.volkov.db.entity.CurrentBookEntity
import com.volkov.db.entity.LastBookEntity

@Database(
    entities = [CurrentBookEntity::class, LastBookEntity::class],
    version = DatabaseConsts.DATABASE_VER,
    exportSchema = false
)
@TypeConverters(DatabaseTypesConverter::class)
abstract class YablDatabase : RoomDatabase() {
    abstract fun currentBookDao(): CurrentBookDao
    abstract fun lastBooksDao(): LastBooksDao

    companion object {
        fun init(context: Context) =
            Room.databaseBuilder(
                context,
                YablDatabase::class.java,
                DatabaseConsts.DATABASE_NAME
            ).fallbackToDestructiveMigration().build()
    }
}