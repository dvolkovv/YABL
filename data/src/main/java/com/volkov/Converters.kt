package com.volkov

import com.volkov.db.entity.CurrentBookEntity
import com.volkov.db.entity.LastBookEntity
import com.volkov.filemanager.FileObject
import java.util.*

fun FileObject.toCurrentBookEntity() =
    CurrentBookEntity(
        playTime = 0,
        title = this.title,
        filepath = this.filePath,
        duration = this.duration
    )

fun CurrentBookEntity.toLastBookEntity(): LastBookEntity =
    LastBookEntity(
        playTime = this.playTime,
        title = this.title,
        duration = this.duration,
        filepath = this.filepath,
        lastTimePlayed = Date()
    )

fun LastBookEntity.toCurrentBookEntity(): CurrentBookEntity =
    CurrentBookEntity(
        playTime = this.playTime,
        title = this.title,
        duration = this.duration,
        filepath = this.filepath
    )