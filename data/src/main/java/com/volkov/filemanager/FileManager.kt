package com.volkov.filemanager

import android.content.Context
import android.database.Cursor
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.provider.OpenableColumns
import java.io.FileNotFoundException


class FileManager constructor(
    private val appContext: Context
) {

    private val contentResolver = appContext.contentResolver

    fun getBookCoverBytes(bookUri: Uri): ByteArray? {
        val mmr = MediaMetadataRetriever()
        mmr.runCatching {
            setDataSource(appContext, bookUri)
            val pic = embeddedPicture
            return if (pic?.isNotEmpty() == true) {
                pic
            } else null
        }
        return null
    }

    @Throws(FileNotFoundException::class)
    fun getFileByUri(uri: Uri): FileObject {
        val title: String
        val duration: Int
        val mmr = MediaMetadataRetriever()
        mmr.runCatching {
            setDataSource(appContext, uri)
        }.onFailure {
            if (it is IllegalArgumentException) {
                throw BookNotFoundOnPathException(uri)
            }
        }
        mmr.run {
            title = extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE) ?: getFileName(uri)
            duration =
                extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toIntOrNull() ?: 0
            release()
        }
        return FileObject(title, duration, uri)
    }

    private fun getFileName(uri: Uri): String {
        val projection = arrayOf(OpenableColumns.DISPLAY_NAME)
        val cursor: Cursor? = contentResolver.query(
            uri,
            projection,
            null,
            null,
            null,
            null
        )
        var fileName = String()

        cursor?.use {
            if (it.moveToFirst()) {
                fileName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
            }
        }
        return fileName
    }
}