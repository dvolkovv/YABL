package com.volkov.filemanager

import android.net.Uri


class BookNotFoundOnPathException(val bookUri: Uri) : Throwable()
