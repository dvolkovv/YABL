package com.volkov.filemanager

import android.net.Uri

data class FileObject(
    val title: String,
    val duration: Int,
    val filePath: Uri
)

